### blog
本博客是我在工作学习中所遇到问道问题的总结，节约大家采坑的时间.

#### Linx
  - [centos docker 指南]()
  - [Mac 下使用dd命令制作Centos USB启动盘](https://github.com/gmw-zjw/blog/issues/12)
  
  

#### nodejs

  - [experss学习之路](https://github.com/gmw-zjw/blog/issues/13)
  - [express body-paeser中间件](https://github.com/gmw-zjw/blog/issues/5)
  - [express express-session 分析](https://github.com/gmw-zjw/blog/issues/6)
  - [nodejs 上传文件获取上传文件进度](https://github.com/gmw-zjw/blog/issues/4)
  - [nodejs 后端生成图像验证码]()
  - [koa2+mysql搭建的个人博客](https://github.com/gmw-zjw/eary-blog)-[教程](https://github.com/gmw-zjw/blog/issues/3)
  - [koa2上传图片到阿里云OSS](https://github.com/gaowei1012/blog/issues/17)
  
  



#### React
 
 - [Antd-搭建一套企业中后端管理系统](https://github.com/gmw-zjw/antd-design-admin)
 - [React虚拟DOM解析](https://zristart.github.io/React%E8%99%9A%E6%8B%9FDOM%E6%B5%85%E6%9E%90.html#more)
  
 - [React的setState之后都做了那些事](https://zristart.github.io/React%E7%9A%84setState%E4%B9%8B%E5%90%8E%E9%83%BD%E5%81%9A%E4%BA%86%E9%82%A3%E4%BA%9B%E4%BA%8B.html#more)
 -[blog](https://github.com/gmw-zjw/blog-pc.git)
  
- [react excle 工作表导出](https://github.com/gaowei1012/blog/issues/20)
 
 #### reactnative
 - [自定义react-native顶部scrollview tab 切换菜单](https://github.com/gaowei1012/blog/issues/14)
 - [react native 中使用美洽客服](https://github.com/gaowei1012/blog/issues/15)
 - [react native 自定义进度条](https://github.com/gaowei1012/blog/issues/18)
 - [react native TextInput 封装](https://github.com/gaowei1012/blog/issues/19)
 - [react native 图片上传]()
 - [react native 自定义图表](https://github.com/gaowei1012/blog/issues/21)
 - [react native 相机扫码实现](https://github.com/gaowei1012/blog/issues/26)
 - [react native 定位](https://github.com/gaowei1012/blog/issues/27)
 

#### javascript
 - [javascript中Event loop事件环机制](https://github.com/gmw-zjw/blog/issues/2)
 - [Promise实现](https://github.com/gmw-zjw/blog/issues/9)
 - [javascript函数防抖](https://github.com/gmw-zjw/blog/issues/10)
 - [javascript函数节流](https://github.com/gmw-zjw/blog/issues/11)
 
 #### mysql
  - [node.js中mysql数据化初始化、表、查询](https://github.com/gaowei1012/blog/issues/16)
  
 #### koa
 - [项目工程化](https://github.com/gaowei1012/blog/issues/22)
 
 #### express
 
 #### redis
 
 #### typeScript
 - [react工程中使用typescript(一), 项目初始化](https://github.com/gaowei1012/blog/issues/23)
 - [react工程中使用typescript(二), 添加请求 axios 、路由、redux](https://github.com/gaowei1012/blog/issues/24)
 - [react hooks 使用 ts](https://github.com/gaowei1012/blog/issues/25)

 #### android
  
 #### flutter
 
 
 
